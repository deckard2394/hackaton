package com.grupo7.hackaton.models;

public class CarModel {

    private String id;
    private String desc;
    private float price;
    private String color;
    private String tipo;

    public CarModel() {
    }

    public CarModel(String id, String desc, float price, String color, String tipo) {
        this.id = id;
        this.desc = desc;
        this.price = price;
        this.color = color;
        this.tipo = tipo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
